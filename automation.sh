#!/bin/bash

dir=test_scripts
Files=./$dir/*

while :
do

# git diff --exit-code $dir
# # git diff param.csv
# code=$?

# Get changes from remote
git fetch

# Check if there are any changes in the relevant directory
if [[ $(git log -p HEAD..FETCH_HEAD | grep $dir) ]] 
	then
		echo "Scripts have been changed"
		git pull
		for file in $Files
		do
			echo "Running $file with Taurus"
			bzt $file -report
		done
	else
		echo "no changes"
fi

sleep 30

done